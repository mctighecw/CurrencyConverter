# README

This is a small project using Xcode and Swift. It is a simple currency converter iOS app.

## App Information

App Name: CurrencyConverter

Created: August 2016

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/CurrencyConverter)

Last updated: 2025-02-11
